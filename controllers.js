var phonecatApp = angular.module('phonecatApp', []);

phonecatApp.controller('PhoneListCtrl', function($scope, $http){

	$scope.title = "Views And Templates";	

	/**$scope.phones = [
		{'name' : 'Nexeus S', 'snippet' : 'Fast just got faster with Nexus S'},
		{'name': 'Motorola Xoom with Wifi', 'snippet': 'The Next Generation'}

	];**/

	$http.get('phones.json').success(function(data) {
		$scope.phones = data;
	});


});
