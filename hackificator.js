angular.module('hackificator', ['ui.bootstrap']);
angular.module('hackificator').controller('HackController', function ($scope, $http) {

	$scope.form = {};		
	$scope.hackify = function() {
	   $http({
		   method: 'post',
		   url: './hackify.php',
		   data : $scope.form,
	   }).success(function(data){
		console.log(data.content);   

		$scope.form.hackcode = data.content;
		$scope.form.log = data.errors;
	   });
   };
});
