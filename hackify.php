<?php

if(!file_exists('/tmp/hack')) {
    mkdir('/tmp/hack');
}
if(!file_exists('/tmp/hack/.hhconfig')) {
    exec('touch /tmp/hack/.hhconfig');
}
$content = json_decode(file_get_contents('php://input'));
file_put_contents('/tmp/hack/test.php', $content->phpcode);
$output = '';
//echo 'start hackificator';
//run hh_client to make sure there are no errors.
$output = exec('/usr/bin/hackificator /tmp/hack 2>&1', $outputAndErrors, $returnValue);
//sleep(1);
if($content->soft_annotation == 1) {
	exec('hh_server --convert /tmp/hack /tmp/hack 2>&1', $sOutputAndErrors, $returnValue);
}

if($content->hard_annotation == 1) {
	exec('/usr/bin/hack_remove_soft_types --harden /tmp/hack/test.php', $other, $ret);
}

//echo '<br />finish hackficator';
$output = array(
	'content' => file_get_contents('/tmp/hack/test.php'), 
	'errors' => $outputAndErrors
);

echo json_encode($output); 

